import {Component, Input, OnInit} from '@angular/core';
import {Cocktail} from "../model/cocktail.model";

@Component({
  selector: 'app-one-cockatil',
  templateUrl: './one-cockatil.component.html',
  styleUrls: ['./one-cockatil.component.css']
})
export class OneCockatilComponent implements OnInit {

  @Input() cocktail! : Cocktail;
  modalOpen = false;

  constructor() { }

  ngOnInit(): void {
  }

  openCheckoutModal() {
    this.modalOpen = true;
  }

  closeCheckoutModal() {
    this.modalOpen = false;
  }

  removeAt(){

  }
}
