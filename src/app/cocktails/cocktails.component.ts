import {Component, OnDestroy, OnInit} from '@angular/core';
import {Cocktail} from "../model/cocktail.model";
import {Subscription} from "rxjs";
import {CocktailService} from "../model/cocktail.service";

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.css']
})
export class CocktailsComponent implements OnInit, OnDestroy {

  cocktails!: Cocktail[];
  cocktailsChangeSubscription!: Subscription;
  cocktailFetchingSubscription!: Subscription;
  loading = false;


  constructor( private cocktailService: CocktailService) { }

  ngOnInit(): void {

    this.cocktailsChangeSubscription = this.cocktailService.cocktailChange.subscribe((cocktails:Cocktail[])=> {
      this.cocktails = cocktails;
    })

    this.cocktailFetchingSubscription = this.cocktailService.cocktailsFetching.subscribe((isFetching: boolean)=> {
      this.loading = isFetching;
    })

    this.cocktailService.fetchCocktail();
  }


  ngOnDestroy() {
    this.cocktailsChangeSubscription.unsubscribe();
    this.cocktailFetchingSubscription.unsubscribe();
  }

}
