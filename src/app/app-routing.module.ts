import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CocktailsComponent} from "./cocktails/cocktails.component";
import {AddCocktailComponent} from "./add-cocktail/add-cocktail.component";

const routes: Routes = [
  {path: '', component: CocktailsComponent},
  {path: 'cocktails/new', component: AddCocktailComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
