export class Cocktail{
  constructor(
    public id: string,
    public name: string,
    public image: string,
    public description: string,
    public type: string,
    public cocktailIngreds: [{ingredName: string,quantity: number,measure: string}],
    public prepGuide: string,
  ) {}
}
