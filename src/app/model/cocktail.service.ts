import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {Cocktail} from "./cocktail.model";
import {HttpClient} from "@angular/common/http";
import {map, tap} from "rxjs/operators";
import {Router} from "@angular/router";


@Injectable()
export class CocktailService {

  cocktailChange = new Subject <Cocktail[]>();
  cocktailUploading = new Subject <boolean>();
  cocktailsFetching = new Subject<boolean>();

  private cocktails: Cocktail[] = [];

  constructor(private http: HttpClient, private router: Router) {}


  addCocktail(body: Object) {
    this.cocktailUploading.next(true);
    return this.http.post('https://js-group-13-default-rtdb.firebaseio.com/test-cocktails.json', body).pipe(
      tap(() => {
        this.cocktailUploading.next(false);
      }, () => {
        this.cocktailUploading.next(false);
      })
    );
  }

  fetchCocktail(){
    this.cocktailsFetching.next(true);
    this.http.get<{[id:string]:Cocktail}>('https://js-group-13-default-rtdb.firebaseio.com/test-cocktails.json')
      .pipe(map (result => {
        return Object.keys(result).map(id=> {
          const cocktailData = result[id];
          return new Cocktail(
            id,
            cocktailData.name,
            cocktailData.image,
            cocktailData.description,
            cocktailData.type,
            cocktailData.cocktailIngreds,
            cocktailData.prepGuide,
          );
        });
      }))
      .subscribe(cocktails=>{
        this.cocktails = cocktails;
        this.cocktailChange.next(this.cocktails.slice());
        this.cocktailsFetching.next(false);
      }, () => {
        this.cocktailsFetching.next(false);
      })
  }







}
