import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddCocktailComponent } from './add-cocktail/add-cocktail.component';
import { OneCockatilComponent } from './one-cockatil/one-cockatil.component';
import { CocktailsComponent } from './cocktails/cocktails.component';
import { NavigationBoardComponent } from './navigation-board/navigation-board.component';
import {CocktailService} from "./model/cocktail.service";
import {HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import {ValidateUrlDirective} from "./validate-url.directive";
import { ModalComponent } from './ui/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    AddCocktailComponent,
    OneCockatilComponent,
    ValidateUrlDirective,
    CocktailsComponent,
    NavigationBoardComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [CocktailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
