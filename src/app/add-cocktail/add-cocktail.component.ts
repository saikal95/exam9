import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {CocktailService} from "../model/cocktail.service";
import {urlValidator} from "../validate-url.directive";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-cocktail',
  templateUrl: './add-cocktail.component.html',
  styleUrls: ['./add-cocktail.component.css']
})
export class AddCocktailComponent implements OnInit {

  cocktailForm!: FormGroup;

  constructor(private cocktailService: CocktailService, private router: Router) {}

  ngOnInit(): void {
    this.cocktailForm = new FormGroup({
      name: new FormControl('', Validators.required),
      image: new FormControl('', [
        Validators.required,
        urlValidator]),
      description: new FormControl(''),
      type: new FormControl('', Validators.required),
      cocktailIngreds: new FormArray([]),
      prepGuide: new FormControl('', Validators.required),
    })
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.cocktailForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }

  addDetails() {
    const cocktailIngreds = <FormArray>this.cocktailForm.get('cocktailIngreds');
    const cocktailGroup = new FormGroup({
      ingredName: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.required),
      measure: new FormControl('', Validators.required),
    })
    cocktailIngreds.push(cocktailGroup);
  }

  getCocktails() {
    const cocktails =  <FormArray>(this.cocktailForm.get('cocktailIngreds'));
    return cocktails.controls;
  }


  sendCocktailRequest(){
    const name = this.cocktailForm.value.name;
    const image = this.cocktailForm.value.image;
    const description = this.cocktailForm.value.description;
    const type = this.cocktailForm.value.type;
    const cocktailIngreds = this.cocktailForm.value.cocktailIngreds;
    const prepGuide  = this.cocktailForm.value.prepGuide;
    const body = {name, image,description,type,cocktailIngreds, prepGuide};

    const next = () => {
      this.cocktailService.fetchCocktail()
      void this.router.navigate(['']);
    };

    this.cocktailService.addCocktail(body).subscribe(next);

  }

  remove(index: number) {
    (<FormArray>this.cocktailForm.controls['cocktailIngreds']).removeAt(index);

  }
}
